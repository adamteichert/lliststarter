#include "list.h"
#include <assert.h>
#include <stdio.h>
#include <stdbool.h>

/*
 * Reads lhs and rhs character by character until either reaches eof.
 * Returns true if the files can be read and the two files match
 * character by character. Returns false if two files either can't be
 * opened or don't match.
 */
bool fileeq(char lhsName[], char rhsName[]) {
    FILE* lhs = fopen(lhsName, "r");
    FILE* rhs = fopen(rhsName, "r");

    // don't compare if we can't open the files
    if (!lhs || !rhs) return false;

    bool match = true;
    // read until both of the files are done or there is a mismatch
    while (!feof(lhs) || !feof(rhs)) {
        if (feof(lhs) ||                  // lhs done first
            feof(rhs) ||                  // rhs done first
            (fgetc(lhs) != fgetc(rhs))) { // chars don't match
            match = false;
            break;
        }
    }
    fclose(lhs);
    fclose(rhs);
    return match;
}

/*
 * Test fileeq on same file, files with same contents, files with
 * different contents and a file that doesn't exist
 */
void test_fileeq() {
    FILE* fptr = fopen("test1.txt", "w");
    fprintf(fptr, "this\nis\na test\n");
    fclose(fptr);

    fptr = fopen("test2.txt", "w");
    fprintf(fptr, "this\nis\na different test\n");
    fclose(fptr);

    fptr = fopen("test3.txt", "w");
    fprintf(fptr, "this\nis\na test\n");
    fclose(fptr);

    assert(fileeq("test1.txt", "test1.txt"));
    assert(fileeq("test2.txt", "test2.txt"));
    assert(!fileeq("test2.txt", "test1.txt"));
    assert(!fileeq("test1.txt", "test2.txt"));
    assert(fileeq("test3.txt", "test3.txt"));
    assert(fileeq("test1.txt", "test3.txt"));
    assert(fileeq("test3.txt", "test1.txt"));
    assert(!fileeq("test2.txt", "test3.txt"));
    assert(!fileeq("test3.txt", "test2.txt"));
    assert(!fileeq("", ""));  // can't open file
}

void test_size() {
    Node n1 = {.data = 3};
    Node n2 = {.data = 2};
    Node n3 = {.data = 6};
    n1.next = &n2;
    n2.next = &n3;
    n3.next = NULL;
    assert(size(&n1) == 3);
    assert(size(&n2) == 2);
    assert(size(&n3) == 1);
    assert(size(NULL) == 0);
}

void checkList(Node* headptr, char* expectedPrint) {
    FILE* fexpected = fopen("fexpected.txt", "w");
    assert(fexpected);
    fprintf(fexpected, expectedPrint);
    fclose(fexpected);
    
    FILE* fobserved = fopen("fobserved.txt", "w");
    assert(fobserved);
    fprintList(fobserved, headptr);
    fclose(fobserved);
    
    assert(fileeq("fexpected.txt", "fobserved.txt"));
}

void testfprintList() {
    Node n1 = {.data = 3};
    Node n2 = {.data = 2};
    Node n3 = {.data = 6};
    n1.next = &n2;
    n2.next = &n3;
    n3.next = NULL;
    checkList(&n1, "3 2 6 ");
}

void testaddFront() {
    Node* headptr = NULL;
    addFront(&headptr, 5);
    checkList(headptr, "5 ");
    addFront(&headptr, 7);
    checkList(headptr, "7 5 ");
    addFront(&headptr, 3);
    checkList(headptr, "3 7 5 ");
    addFront(&headptr, 3);
    checkList(headptr, "3 3 7 5 ");

}

int main(void) {
    test_size();
    testfprintList();
    testaddFront();
    return 0;
}
