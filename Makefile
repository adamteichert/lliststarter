
CC=gcc
CFLAGS=-Wall -Wextra -pedantic -std=c99
progs=test_list

test: test_list
	@echo "Testing..."
	./test_list
	@echo "Passed all tests."

list.o: list.c list.h
test_list: test_list.c list.o

clean:
	rm -f *.o $(progs)
