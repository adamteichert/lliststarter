#include "list.h"
#include <stdio.h>
#include <stdlib.h>

int size(const Node* headptr) {
    if (headptr) {
        return 1 + size(headptr->next);
    } else {
        return 0;
    }
}

void fprintList(FILE* outfile, const Node* headptr) {
    for (; headptr; headptr = headptr->next) {
        fprintf(outfile, "%d ", headptr->data);
    }
}

void fprintListR(FILE* outfile, const Node* headptr) {
    if (headptr) {
        fprintf(outfile, "%d ", headptr->data);
        fprintList(outfile, headptr->next);
    }
}

void fprintListRev(FILE* outfile, const Node* headptr) {
    if (headptr) {
        fprintListRev(outfile, headptr->next);
        fprintf(outfile, "%d ", headptr->data);
    }
}

void printList(const Node* headptr) {
    fprintList(stdout, headptr);
}

void printListR(const Node* headptr) {
    fprintListR(stdout, headptr);
}

void printListRev(const Node* headptr) {
    fprintListRev(stdout, headptr);
}

void addFront(Node** headptrrf, int data) {
    Node* oldheadptr = *headptrrf;          //-to keep the *s straight
    Node* newheadptr = malloc(sizeof(Node));//-get a new node
    newheadptr->data = data;                //-populate with data
    newheadptr->next = oldheadptr;          //-point next to old head
    *headptrrf = newheadptr;                //-update the head by ref
}

int deleteFront(Node** headptrref) {
    Node* oldheadptr = *headptrref;
    if (oldheadptr) {
        *headptrref = oldheadptr->next;
        free(oldheadptr);
        return 1;
    } else {
        return 0;
    }
}

int delete(Node** headptrref, int todelete) {
    for (; *headptrref; headptrref = &((*headptrref)->next)) {
        if ((*headptrref)->data == todelete) {
            return deleteFront(headptrref);
        }
    }
    return 0;
}

int deleteR(Node** headptrref, int todelete) {
    Node* oldheadptr = *headptrref;
    if (!oldheadptr) {                  // if list is empty, give up
        return 0;                       
    } else if (oldheadptr->data == todelete) {
        return deleteFront(headptrref); // if frst node matches, rm it
    } else {                            // otherwise, recurse
        return deleteR(&oldheadptr->next, todelete); 
    }
}

void clearList(Node** headptrref) {
    Node* oldheadptr = *headptrref;
    if (oldheadptr) {                // if the list isn't empty
        clearList(&oldheadptr->next);// clear the rest of the list
        free(oldheadptr);            // free the remaining first node
        *headptrref = NULL;          // make the headptr point to NULL
    }
}

// // Alternative version of clearList that uses deleteFront
//void clearList(Node** headptrref) {
//    while (*headptrref) {
//        deleteFront(headptrref);
//    }
//}

Node* find(Node* headptr, int tofind) {
    if (headptr == NULL || headptr->data == tofind) {
        return headptr;
    } else {
        return find(headptr->next, tofind);
    }
}

int replace(Node* headptr, int old, int new) {
    Node* toChange = find(headptr, old);
    if (toChange) {
        toChange->data = new;
        return 1;
    } else {
        return 0;
    }
}
